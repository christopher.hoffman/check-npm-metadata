const { RegistryClient } = require('package-metadata');
const { writeFile } = require('fs/promises');


/** Removes pieces of the metadata which give false-flags for secrets */
async function processLineByLine() {
  const fs = require('fs');
  const readline = require('readline');

  const fileStream = fs.createReadStream('output.json');
  const writeSteam = fs.createWriteStream('cleanedOutput.json')

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    //console.log(`Line from file: ${line}`);
    const regexMatch = new RegExp(/integrity|shasum|npm-signature|\"sig\"|\"keyid\": \"SHA256|gitHead/)
    if (!regexMatch.test(line)) {
      writeSteam.write(line + "\n");
    }
  }
}

const args = process.argv.slice(2);

const moduleName = args[0];

RegistryClient.getMetadata(moduleName, {allVersions: true, fullMetadata: true})
  .then((metadata) => {
    // Save the metadata
    return writeFile('./output.json',  JSON.stringify(metadata, null, 2))
  }).then( () => {
    // Clean the metadata
    return processLineByLine()
  }
  ).then(() => {
    console.log("Completed")
  });