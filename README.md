# Checking Npm Metadata

## Step 1: Getting metadata
Run `node check-npm-metadata.js "<ModuleName>"`, which will generate two files:

- `output.json`: The raw metadata from npm
- `cleanedOutput.json`: A cleaned version with known npm hashes/checksums removes that would trigger false-positives with secret detection.

## Step 2: Running output through Detect Secrets
Yelp has a python package called `detect-secrets` which can scan files to detect various forms of secrets. It has been converted into a docker container `lirantal/detect-secrets`.
https://github.com/lirantal/docker-detect-secrets

After getting the `cleanedOutput.json` from the above script, run it through the docker container scanner.

```
docker run -it --rm --name detect-secrets --volume `pwd`:/usr/src/app lirantal/detect-secrets "cleanedOutput.json"
```

If the only output is `[scan]  INFO    Checking file: cleanedOutput.json`, then there are no secrets detected.

Otherwise, you will see output describing the potential secret locations:
```
ERROR: Potential secrets about to be committed to git repo!

Secret Type: Hex High Entropy String
Location:    cleanedOutput.json:68942

Secret Type: Hex High Entropy String
Location:    cleanedOutput.json:73149

Secret Type: Hex High Entropy String
Location:    cleanedOutput.json:75148
...
```
